package com.example.davaleba

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.widget.Button
import android.widget.TextView
import org.w3c.dom.Text

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }
    private fun init(){
        val generateRandomNumberButton =findViewById<Button>(R.id.generateRandomNumberButton)
        val randomNumberTextView = findViewById<TextView>(R.id.randomNumberTextView)
        generateRandomNumberButton.setOnClickListener {
            val number = (-100..100).random()
            d("randomNumber","This is random number $number")
            randomNumberTextView.text = divisionOnFive(number).toString()
        }
    }
    private fun divisionOnFive(num:Int):String{
        return if(num%5==0 && num/5>0){
            "YES"
        }else{
            "NO"
        }

    }
}